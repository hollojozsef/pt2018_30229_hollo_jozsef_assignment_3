package model;
/**
 * 
 * @author Ioji
 *Clasa Product contine atributele productID,numeProdus si cantitate, 
 *din nou metodele sunt cele de baza: gettere, settere si toString.
 */
public class Product {
	public String productName;
	public int productID;
	public int cantitate;

	public Product(String productName, int productID, int cantitate) {
		this.productName = productName;
		this.productID = productID;
		this.cantitate = cantitate;
	}

	public Product() {
		// TODO Auto-generated constructor stub
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getProductID() {
		return productID;
	}

	public void setProductID(int productID) {
		this.productID = productID;
	}

	public int getCantitate() {
		return cantitate;
	}

	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}

}
