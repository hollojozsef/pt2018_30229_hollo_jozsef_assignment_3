package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.cj.jdbc.PreparedStatement;
/**
 * 
 * @author Ioji
 *Clasa Client contine clientID si numeClient ca si variabile instante, iar ca si 
 *metode gettere settere si toString. Nu a fost necesaraintroducerea altor metode in aceasta clasa.
 */
public class Client {
	public String clientName;
	public int clientID;

	public Client(String clientName, int clientID) {
		this.clientName = clientName;
		this.clientID = clientID;
	}

	public Client() {
		// TODO Auto-generated constructor stub
	}
/**
 * 
 * @return numele clientului
 */
	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public int getClientID() {
		return clientID;
	}

	public void setClientID(int clientID) {
		this.clientID = clientID;
	}

	@Override
	public String toString() {
		return "clientName=" + clientName + ", clientID=" + clientID + "\n";
	}
	

}
