package model;
/**
 * 
 * @author Ioji
 *Clasa Comand are ca si atribute comandID, productID, clientID si cantitate. Aceasta clasa se bazeaza pe clasa 
 *Cient si Product deoarece in baza de date am creat o legatura intre Client, Produs si Comanda. 
 *O comanda nu poate exista fara un client si un produs insa un client poate exista fara produse si invers.
 *
 *
 */
public class Comand {
	public int comandID;
	public int productID;
	public int clientID;
	public int cantitate;

	public Comand(int comandID, int productID, int clientID, int cantitate) {
		this.comandID = comandID;
		this.productID = productID;
		this.clientID = clientID;
		this.cantitate = cantitate;
	}

	public Comand() {
		// TODO Auto-generated constructor stub
	}

	public int getComandID() {
		return comandID;
	}

	public void setComandID(int comandID) {
		this.comandID = comandID;
	}

	public int getProductID() {
		return productID;
	}

	public void setProductID(int productID) {
		this.productID = productID;
	}

	public int getClientID() {
		return clientID;
	}

	public void setClientID(int clientID) {
		this.clientID = clientID;
	}

	public int getCantitate() {
		return cantitate;
	}

	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}

}
