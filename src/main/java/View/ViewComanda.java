package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import DAO.ComandDAO;

import javax.swing.SpringLayout;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ViewComanda extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;

	public ViewComanda() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		SpringLayout sl_contentPane = new SpringLayout();
		contentPane.setLayout(sl_contentPane);
		
		textField = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, textField, 10, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, textField, -85, SpringLayout.EAST, contentPane);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, textField_1, 18, SpringLayout.SOUTH, textField);
		sl_contentPane.putConstraint(SpringLayout.EAST, textField_1, 0, SpringLayout.EAST, textField);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblIdProdus = new JLabel("ID Produs");
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblIdProdus, 0, SpringLayout.NORTH, textField);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblIdProdus, 87, SpringLayout.WEST, contentPane);
		contentPane.add(lblIdProdus);
		
		JLabel lblIdClient = new JLabel("ID Client");
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblIdClient, 0, SpringLayout.NORTH, textField_1);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblIdClient, 0, SpringLayout.WEST, lblIdProdus);
		contentPane.add(lblIdClient);
		
		textField_2 = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, textField_2, 26, SpringLayout.SOUTH, textField_1);
		sl_contentPane.putConstraint(SpringLayout.WEST, textField_2, 0, SpringLayout.WEST, textField);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblCantitate = new JLabel("Cantitate");
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblCantitate, 0, SpringLayout.NORTH, textField_2);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblCantitate, 0, SpringLayout.WEST, lblIdProdus);
		contentPane.add(lblCantitate);
		
		JButton btnAdaugaComanda = new JButton("Adauga Comanda");
		btnAdaugaComanda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ComandDAO c=new ComandDAO();
				c.addComand(Integer.parseInt(textField.getText()), Integer.parseInt(textField_1.getText()), Integer.parseInt(textField_2.getText()));
			}
		});
		sl_contentPane.putConstraint(SpringLayout.SOUTH, btnAdaugaComanda, -45, SpringLayout.SOUTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, btnAdaugaComanda, 0, SpringLayout.EAST, lblIdProdus);
		contentPane.add(btnAdaugaComanda);
		
		JButton btnBack = new JButton("BACK");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				View v=new View();
				v.setVisible(true);
			}
		});
		sl_contentPane.putConstraint(SpringLayout.NORTH, btnBack, 0, SpringLayout.NORTH, btnAdaugaComanda);
		sl_contentPane.putConstraint(SpringLayout.EAST, btnBack, 0, SpringLayout.EAST, textField);
		contentPane.add(btnBack);
		
		JButton btnVizualizareTabel = new JButton("Vizualizare tabel");
		btnVizualizareTabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ComandDAO c=new ComandDAO();
				c.findAllComands();
			}
		});
		sl_contentPane.putConstraint(SpringLayout.WEST, btnVizualizareTabel, 150, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, btnVizualizareTabel, -10, SpringLayout.SOUTH, contentPane);
		contentPane.add(btnVizualizareTabel);
	}
}
