package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.SpringLayout;
import java.awt.CardLayout;
import net.miginfocom.swing.MigLayout;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

import DAO.ClientDAO;

import javax.swing.BoxLayout;
import java.awt.GridBagLayout;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ViewClient extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;

	public ViewClient() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		SpringLayout sl_contentPane = new SpringLayout();
		contentPane.setLayout(sl_contentPane);
		
		textField = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, textField, 33, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, textField, -126, SpringLayout.EAST, contentPane);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblIdClient = new JLabel("ID Client");
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblIdClient, 0, SpringLayout.NORTH, textField);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblIdClient, 77, SpringLayout.WEST, contentPane);
		contentPane.add(lblIdClient);
		
		textField_1 = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, textField_1, 22, SpringLayout.SOUTH, textField);
		sl_contentPane.putConstraint(SpringLayout.WEST, textField_1, 0, SpringLayout.WEST, textField);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblNume = new JLabel("Nume");
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblNume, 0, SpringLayout.NORTH, textField_1);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblNume, 0, SpringLayout.WEST, lblIdClient);
		contentPane.add(lblNume);
		
		JButton btnAdaugaClient = new JButton("Adauga Client");
		btnAdaugaClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ClientDAO c=new ClientDAO();
				c.addClient(textField_1.getText());
			}
		});
		sl_contentPane.putConstraint(SpringLayout.WEST, btnAdaugaClient, 61, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, btnAdaugaClient, -56, SpringLayout.SOUTH, contentPane);
		contentPane.add(btnAdaugaClient);
		
		JButton btnStergeClient = new JButton("Sterge Client");
		btnStergeClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ClientDAO c=new ClientDAO();
				c.deleteClient(Integer.parseInt(textField.getText()));
			}
		});
		sl_contentPane.putConstraint(SpringLayout.WEST, btnStergeClient, 86, SpringLayout.EAST, btnAdaugaClient);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, btnStergeClient, 0, SpringLayout.SOUTH, btnAdaugaClient);
		contentPane.add(btnStergeClient);
		
		JButton btnAfisareTabel = new JButton("Afisare Tabel");
		btnAfisareTabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ClientDAO c=new ClientDAO();
				c.findAllClient();
			}
		});
		sl_contentPane.putConstraint(SpringLayout.WEST, btnAfisareTabel, 0, SpringLayout.WEST, btnAdaugaClient);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, btnAfisareTabel, -10, SpringLayout.SOUTH, contentPane);
		contentPane.add(btnAfisareTabel);
		
		JButton btnBack = new JButton("BACK");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				View v=new View();
				v.setVisible(true);
			}
		});
		sl_contentPane.putConstraint(SpringLayout.SOUTH, btnBack, 0, SpringLayout.SOUTH, btnAfisareTabel);
		sl_contentPane.putConstraint(SpringLayout.EAST, btnBack, 0, SpringLayout.EAST, btnStergeClient);
		contentPane.add(btnBack);
	}

}
