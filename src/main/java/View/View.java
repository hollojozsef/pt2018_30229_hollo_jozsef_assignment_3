package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
/**
 * In pachetul View avem clasa View care poate fi vazuta ca si un controller deoarece in 
 * ea se creaza prima fereastra a interfetei grafice de unde utilizatorul poate sa intre in client, 
 * produs sau adauga comanda. Am creat pentru fiecare din aceste noi ferestre care se deschid cate o clasa noua. 
 * @author Ioji
 *
 */
public class View extends JFrame {

	private JPanel contentPane;
	public View() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JButton btnProduse = new JButton("PRODUSE");
		btnProduse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ViewProdus v=new ViewProdus();
				v.setVisible(true);
			}
		});
		contentPane.add(btnProduse, BorderLayout.WEST);
		
		JButton btnClienti = new JButton("CLIENTI");
		btnClienti.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					ViewClient frame = new ViewClient();
					frame.setVisible(true);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		contentPane.add(btnClienti, BorderLayout.EAST);
		
		JButton btnAdaugaComanda = new JButton("ADAUGA COMANDA");
		btnAdaugaComanda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					ViewComanda frame = new ViewComanda();
					frame.setVisible(true);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		contentPane.add(btnAdaugaComanda, BorderLayout.CENTER);
	}

}
