package DAO;

import java.awt.ScrollPane;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.mysql.cj.jdbc.PreparedStatement;

import connection.ConnectionFactory;
import model.Client;
import model.Product;
/**
 * 
 * @author Ioji
 *Clasa ProductDAO contine patru metode de acces la baza de date.
 * Toate metodele stabilesc o conexiune la baza de date dupa care o inchid. 
 */
public class ProductDAO {
	public final static String findStatementString = "SELECT * FROM product where productID=?";
	public final static String insertStatementString = "INSERT INTO product VALUES (null, ?, ?)";
	public final static String deleteStatementString = "DELETE FROM product where productID=?";
	public final static String findAllStatementString = "SELECT * FROM product";
/**
 *  Metoda findProduct executa un query da cautare a unui produs in baza de date dupa ID. 
 * @param productID
 * @return
 */
	public static Product findProduct(int productID) {
		Product toReturn = new Product();
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = (PreparedStatement) dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, productID);
			rs = findStatement.executeQuery();
			rs.next();
			if (rs.first()) {
				toReturn.setProductID(Integer.parseInt(rs.getString("productID")));
				toReturn.setProductName(rs.getString("productName"));
				toReturn.setCantitate(Integer.parseInt(rs.getString("cantitate")));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		ConnectionFactory.close(dbConnection);
		ConnectionFactory.close(rs);
		return toReturn;

	}
/**
 * Metoda addProduct ne adauga un produs cu atributele numa si id la baza de date.
 * @param nume
 * @param cantitate
 */
	public void addProduct(String nume, int cantitate) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = (PreparedStatement) dbConnection.prepareStatement(insertStatementString);

			findStatement.setString(1, nume);
			findStatement.setLong(2, cantitate);

			findStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		ConnectionFactory.close(dbConnection);
	}
/**
 *  Metoda deleteProduct sterge produsul cu id-ul dat ca si parametru din baza de date. 
 * @param productID
 */
	public void deleteProduct(int productID) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;

		try {
			findStatement = (PreparedStatement) dbConnection.prepareStatement(deleteStatementString);
			findStatement.setLong(1, productID);
			findStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	/**
	 *  In final metoda findAllProduct ne creaza un frame nou in care ne sunt afisate toate lementele aflate in acel momentin tabela product.
	 */
	public void findAllProducts() {
		Object [][] data=new Object[10][10];
		Client toReturn = new Client();
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
	
		try {
			findStatement = (PreparedStatement) dbConnection.prepareStatement(findAllStatementString);
			rs = findStatement.executeQuery();
			
			int i=0;
			while(rs.next()) {
				data[i][2]=rs.getString("cantitate");
				data[i][1]=	rs.getString("productName");
				data[i][0]=rs.getString("productID");
				i++;
			
				
			}
		

		} catch (SQLException e) {
			e.printStackTrace();
		}
		Field[] fields=new Product().getClass().getFields();
		String[] s=new String[3];
		s[0]=fields[0].getName();
		s[1]=fields[1].getName();
		s[2]=fields[2].getName();
		 JFrame f= new JFrame();
		 JTable table = new JTable(data,s);
		 f.setBounds(100, 100, 450, 300);
		 table.setFillsViewportHeight(true);
		f.add(table);
		table.setVisible(true);
		f.add(new JScrollPane(table));
		f.setVisible(true);
		ConnectionFactory.close(dbConnection);
		ConnectionFactory.close(rs);
		

	}
	
	
	
	
}
