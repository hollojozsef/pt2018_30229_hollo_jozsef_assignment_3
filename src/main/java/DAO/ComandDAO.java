package DAO;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.mysql.cj.jdbc.PreparedStatement;

import connection.ConnectionFactory;
import model.Client;
import model.Comand;
/**
 * 
 * @author Ioji
 *Clasa ComandDAO are la fel ca si celelate clase de pana acum ca si atribute patru stringuri care contin 
 *query-urile necesare la lucrul cu baza de date ins pe langa acestea mai contine si o variabila statica int 
 *count pe care am adaugat-o pentru a putea updata cantitatea din tabela de produse si pentru a afisa un mesaj 
 *de eroare in cazul in care nu exista destule produse cate sun cerute in comanda. Metoda addComand adauga la
 * tabela de comenzi o  comanda noua doar in cazul in care extiste desule produse in tabela product, in caz 
 * contrar va afisa un mesaj de eroare si v-a intrerupe executia query-ului, aceasta se foloseste atunci cand 
 * este apelatade  metoda update product care scade din catitatea produsului aflat in comada cantitatea comandata.
 *  Metoda deleteOrder sterge o comanda din baza de date. Metoda findAllComands ne creaza un JFrame nou in interfata 
 *  grafica in care ne afiseaza toate comenzile aflate in tabela comenzi la momentul respective.
 */
public class ComandDAO {
	public final static String insertStatementString = "INSERT INTO comand VALUES (null, ?, ?,?)";
	public final static String deleteStatementString = "DELETE FROM comand where productID=?";
	public final static String updateProduct = "UPDATE  product SET cantitate=? WHERE productID=?";
	public final static String findAllStatementString = "SELECT * FROM comand";
public static int count=0;
/**
 * Metoda addComand adauga la tabela de comenzi o  comanda noua doar in cazul in care extiste desule produse in 
 * tabela product, in caz contrar va afisa un mesaj de eroare si v-a intrerupe executia query-ului, aceasta se
 *  foloseste atunci cand este apelatade  metoda update product care scade din catitatea produsului aflat in comada 
 *  cantitatea comandata
 * @param productID
 * @param clientID
 * @param cantitate
 */
	public void addComand(int productID, int clientID, int cantitate) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;

		try {
			findStatement = (PreparedStatement) dbConnection.prepareStatement(insertStatementString);

			findStatement.setLong(1, productID);
			findStatement.setLong(2, clientID);
			findStatement.setLong(3, cantitate);
			count=0;
			this.updateProdus(productID, cantitate);
			if(count==0)
			findStatement.executeUpdate();
			else
				return;
			
		} catch (SQLException e) {
			System.out.println("ClientID/PruductID gresite!");
			e.printStackTrace();
		}
		ConnectionFactory.close(dbConnection);
	}
/**
 * Updateaza cantitatea din tabela product
 * @param productID
 * @param cantitate
 */
	public void updateProdus(int productID, int cantitate) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ProductDAO p = new ProductDAO();
		try {
			findStatement = (PreparedStatement) dbConnection.prepareStatement(updateProduct);
			int a = p.findProduct(productID).getCantitate();

			findStatement.setLong(2, productID);
			System.out.println("update");
			if(a-cantitate<0) {
				count=1;
				JOptionPane optionPane = new JOptionPane("Cantitatea introdusa este prea mare!", JOptionPane.ERROR_MESSAGE);    
				JDialog dialog = optionPane.createDialog("Failure");
				dialog.setAlwaysOnTop(true);
				dialog.setVisible(true);
				 return;
				 
			}
			
			findStatement.setLong(1, a - cantitate);
			findStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Cantitate prea mare!");
			// e.printStackTrace();
		}
		ConnectionFactory.close(dbConnection);
	}
	/**
	 * . Metoda deleteOrder sterge o comanda din baza de date
	 * @param productID
	 */
	void deleteOrder(int productID) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;

		try {
			findStatement = (PreparedStatement) dbConnection.prepareStatement(deleteStatementString);
			findStatement.setLong(1, productID);
			findStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Metoda findAllComands ne creaza un JFrame nou in 
	 * interfata grafica in care ne afiseaza toate comenzile aflate in tabela comenzi la momentul respective.
	 */
	public void findAllComands() {
		Object [][] data=new Object[10][10];
		Client toReturn = new Client();
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
	
		try {
			findStatement = (PreparedStatement) dbConnection.prepareStatement(findAllStatementString);
			rs = findStatement.executeQuery();
			
			int i=0;
		
			while(rs.next()) {
				data[i][3]=rs.getString("cantitate");
				data[i][2]=rs.getString("clientID");
				data[i][1]=	rs.getString("productID");
				data[i][0]=rs.getString("comandID");
				i++;
				//System.out.println(toReturn.getClientID()+"   "+toReturn.getClientName()+"   ");
				//toReturn.toString();
				
			}
		

		} catch (SQLException e) {
			e.printStackTrace();
		}
		Field[] fields=new Comand().getClass().getFields();
		String[] s=new String[4];
		s[0]=fields[0].getName();
		s[1]=fields[1].getName();
		s[2]=fields[2].getName();
		s[3]=fields[3].getName();
		 JFrame f= new JFrame();
		 JTable table = new JTable(data,s);
		 f.setBounds(100, 100, 450, 300);
		 table.setFillsViewportHeight(true);
		f.add(table);
		table.setVisible(true);
		f.add(new JScrollPane(table));
		f.setVisible(true);
		ConnectionFactory.close(dbConnection);
		ConnectionFactory.close(rs);
		

	}

}
