package DAO;

import java.awt.BorderLayout;
import java.awt.Container;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import com.mysql.cj.jdbc.PreparedStatement;

import connection.ConnectionFactory;
import model.Client;
/**
 * 
 * @author Ioji
 *Clasa ClientDAO are ca si atribute patru stringuri care reprezinta qury-urile necesare 
 *pentru lucrul ca baza de date. Metoda findAllClient ne returneaza in interfata grafica 
 *un frame nou in care  ne va fi afisata tabela client cu toate datele aflate in ea.
 * Metoda findClient ne returneaza clientul cu id-ul dat ca si parametru sin baza de date.
 *  Metoda addClient ne adauga la baza de date clientul cu atributele date ca si parametru. 
 *In final metoda deleteClient ne sterge clientul cu id-ul droit din baza de date.
 */
public class ClientDAO {
	public final static String findStatementString = "SELECT * FROM client where clientID=?";
	public final static String findAllStatementString = "SELECT * FROM client";
	public final static String deleteStatementString = "DELETE FROM client where clientID=?";
	public final static String insertStatementString = "INSERT INTO client VALUES (null, ?)";
	/**
	 *Afiseaza intr-o tabela noua toti clientii din baza de date
	 *
	 * @return Client
	 */
	public static Client findAllClient() {
		Object [][] data=new Object[10][10];
		Client toReturn = new Client();
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
	
		try {
			findStatement = (PreparedStatement) dbConnection.prepareStatement(findAllStatementString);
			rs = findStatement.executeQuery();
			
			int i=0;
		
			while(rs.next()) {
				toReturn.setClientName(rs.getString("clientName"));
				
				
				toReturn.setClientID(Integer.parseInt(rs.getString("clientID")));
				data[i][1]=	rs.getString("clientName");
				data[i][0]=rs.getString("clientID");
				i++;
				System.out.println(toReturn.getClientID()+"   "+toReturn.getClientName()+"   ");
				//toReturn.toString();
				
			}
		

		} catch (SQLException e) {
			e.printStackTrace();
		}
		Field[] fields=new Client().getClass().getFields();
		String[] s=new String[2];
		s[0]=fields[0].getName();
		s[1]=fields[1].getName();
	
		 JFrame f= new JFrame();
		 JTable table = new JTable(data,s);
		 f.setBounds(100, 100, 450, 300);
		 table.setFillsViewportHeight(true);
		f.add(table);
		table.setVisible(true);
		f.add(new JScrollPane(table));
		f.setVisible(true);
		ConnectionFactory.close(dbConnection);
		ConnectionFactory.close(rs);
		return toReturn;

	}
	
	/**
	 * Cauta clientul cu id-ul ClientID
	 * @param clientID
	 * @return Client
	 */
	

	public static Client findClient(int clientID) {
		Client toReturn = new Client();
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = (PreparedStatement) dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, clientID);
			rs = findStatement.executeQuery();
			rs.next();
			if (rs.first())
				toReturn.setClientName(rs.getString("clientName"));
			System.out.println(toReturn.getClientName());

		} catch (SQLException e) {
			e.printStackTrace();
		}
		ConnectionFactory.close(dbConnection);
		ConnectionFactory.close(rs);
		return toReturn;

	}
/**
 * Adauga un client la baza de date
 * @param nume
 */
	public void addClient(String nume) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = (PreparedStatement) dbConnection.prepareStatement(insertStatementString);
			// findStatement.setLong(1, id);
			findStatement.setString(1, nume);

			findStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void deleteClient(int clientID) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;

		try {
			findStatement = (PreparedStatement) dbConnection.prepareStatement(deleteStatementString);
			findStatement.setLong(1, clientID);
			findStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
