package BLL;

import java.util.NoSuchElementException;

import DAO.ClientDAO;
import model.Client;

/**
 * Logica client
 * @author Ioji
 *
 */
public class ClientBLL {
/**
 * Verifica daca exista clientul in baza de date, daca nu, am aruncat o exceptie.
 * @param id
 * @return
 */
public Client findClientById(int id) {
	Client c=ClientDAO.findClient(id);
	if(c==null) {
		throw new NoSuchElementException("The client with id =" + id + " was not found!");
	}
	return c;
		}
}
