package BLL;

import java.util.NoSuchElementException;

import DAO.ProductDAO;
import model.Product;
/**
 * Logica Produs
 * @author Yo-G
 *
 */
public class ProductBLL {
	/**
	 *  Verifica daca exista produsul in baza de date, daca nu, am aruncat o exceptie.
	 * @param id
	 * @return
	 */
	public Product findProductById(int id) {
		Product c=ProductDAO.findProduct(id);
		if(c==null) {
			throw new NoSuchElementException("The product with id =" + id + " was not found!");
		}
		return c;
		}
}
